package com.company;

import java.io.FileReader;
import java.io.File;
import java.util.*;
import java.util.concurrent.CountedCompleter;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        Manager manager = new Manager();
        manager.readFromFile("E:\\Project\\Semestr 5 Cross-programming\\Lab 3\\list.txt");
        manager.tablePrint();
        manager.allCoursesVisitors();
        manager.sameListeners();
        manager.biggerCoursesCountTeachers();
    }
}
