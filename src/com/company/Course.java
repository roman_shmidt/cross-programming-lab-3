package com.company;
import java.util.*;

public class Course {
    String courseName;
    String teacherName;
    ArrayList<String> studentsNames;

    Course(String courseName, String teacherName, ArrayList<String> studentsNames)
    {
        this.courseName = courseName;
        this.teacherName = teacherName;
        this.studentsNames = studentsNames;
    }

    void setCourseName(String courseName)
    {
        this.courseName = courseName;
    }

    String getCourseName()
    {
        return this.courseName;
    }

    void setTeacherName(String teacherName)
    {
        this.teacherName = teacherName;
    }

    String getTeacherName()
    {
        return this.teacherName;
    }

    void setStudentsNames(ArrayList<String> studentsNames)
    {
        this.studentsNames = studentsNames;
    }

    ArrayList<String> getStudentsNames()
    {
        return this.studentsNames;
    }
}
