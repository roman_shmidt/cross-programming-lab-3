package com.company;

import java.io.File;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.Set;

public class Manager {
    private static final String pathname1 = "E:\\Project\\Semestr 5 Cross-programming\\Lab 3\\list.txt";
    private static final String pathname1Mixed = "E:\\Project\\Semestr 5 Cross-programming\\Lab 3\\list.txt";
    private static final String pathname2 = "E:\\Project\\Semestr 5 Cross-programming\\Lab 3\\list2.txt";
    private ArrayList<Course> courseArrayList = new ArrayList<>();
    private ArrayList<Course> courseArrayList2 = new ArrayList<>();

    public void biggerCoursesCountTeachers()
    {
        try
        {
            Scanner scanner = new Scanner(new File(("E:\\Project\\Semestr 5 Cross-programming\\Lab 3\\list2.txt")));
            String line;
            while (scanner.hasNextLine())
            {
                line = scanner.nextLine();
                if(line.equals("*"))
                {
                    if(scanner.hasNextLine())
                    {
                        line = scanner.nextLine();
                        String teacherName = line;
                        String courseName = scanner.nextLine();
                        ArrayList<String> studentsName = new ArrayList<>();
                        while(!(line = scanner.nextLine()).equals("*"))
                        {
                            studentsName.add(line);
                        }
                        courseArrayList2.add(new Course(courseName, teacherName, studentsName));
                    }
                }
            }
            scanner.close();
        }
        catch (Exception exception) {
            System.out.println("There is exception in file reading: " + exception);
        }
        ArrayList<Course> globalCourseList = new ArrayList<>();
        for (var elem: courseArrayList)
        {
            if(globalCourseList.size() !=0)
            {
                int good = 0;
                for(var elem2: globalCourseList)
                {
                    if(!elem2.courseName.equals(elem.courseName) || !elem2.teacherName.equals(elem.teacherName))
                    {
                        good++;
                    }
                }
                if(good == globalCourseList.size())
                {
                    globalCourseList.add(elem);
                }
            }
            else
            {
                globalCourseList.add(elem);
            }
        }
        for (var elem: courseArrayList2)
        {
            if(globalCourseList.size() !=0)
            {
                int good = 0;
                for(var elem2: globalCourseList)
                {
                    if(!elem2.courseName.equals(elem.courseName) || !elem2.teacherName.equals(elem.teacherName))
                    {
                        good++;
                    }
                }
                if(good == globalCourseList.size())
                {
                    globalCourseList.add(elem);
                }
            }
            else
            {
                globalCourseList.add(elem);
            }
        }
        Hashtable<String, ArrayList<String>> teachersTableCourses = new Hashtable<>();
        for(var elem: globalCourseList)
        {
            if(!teachersTableCourses.containsKey(elem.teacherName))
            {
                ArrayList<String> infoList = new ArrayList<>();
                String infoLower = elem.courseName;
                infoList.add(infoLower);
                teachersTableCourses.put(elem.teacherName, infoList);
            }
            else
            {
                ArrayList<String> infoList = teachersTableCourses.get(elem.teacherName);
                if(!infoList.contains(elem.courseName))
                {
                    infoList.add(elem.courseName);
                }
            }
        }
        double average = 0;
        for(var elem: teachersTableCourses.keySet())
        {
            average += teachersTableCourses.get(elem).size();
        }
        average /= teachersTableCourses.size();
        ArrayList<String> averageBiggerCollectionOfTeachers = new ArrayList<>();
        for(var elem: teachersTableCourses.keySet())
        {
            if(teachersTableCourses.get(elem).size() >= average)
            {
                averageBiggerCollectionOfTeachers.add("Count of courses: " + teachersTableCourses.get(elem).size() + " Teacher: " + elem);
            }
        }
        System.out.println("Average count courses is: " + average);
        for(var elem: averageBiggerCollectionOfTeachers)
        {
            System.out.println(elem);
        }
        System.out.println();
    }

    public void sameListeners()
    {
        Course firstChoose;
        Course secondChoose;
        Hashtable<String, ArrayList<String[]>> studentsInfoTable = new Hashtable<>();

        System.out.print("Choose list to check: ");
        Scanner scanner = new Scanner(System.in);
        int choose = scanner.nextInt();
        ArrayList <Course> choosenCourseArrayList;
        if(choose == 1)
        {
            choosenCourseArrayList = courseArrayList;
        }
        else
        {
            choosenCourseArrayList = courseArrayList2;
        }
        for(var elem: choosenCourseArrayList)
        {
            for(var elem2: elem.studentsNames)
            {
                if(!studentsInfoTable.containsKey(elem2))
                {
                    ArrayList<String[]> infoList = new ArrayList<>();
                    String[] infoLower = {elem.courseName, elem.teacherName};
                    infoList.add(infoLower);
                    studentsInfoTable.put(elem2, infoList);
                }
                else
                {
                    ArrayList<String[]> infoList = studentsInfoTable.get(elem2);
                    int count = 0;
                    for(var elem3: infoList)
                    {
                        if(!elem3[0].equals(elem.courseName) || !elem3[1].equals(elem.teacherName))
                        {
                            count++;
                        }
                    }
                    if(count == infoList.size())
                    {
                        infoList.add(new String[]{elem.courseName, elem.teacherName});
                    }
                }
            }
        }
        int elemCount = 0;
        System.out.println("Choose courses to find students: ");
        Course[] coursesArray = new Course[choosenCourseArrayList.size()];
        for(var elem: choosenCourseArrayList)
        {
            coursesArray[elemCount] = elem;
            System.out.println(elemCount+1 +". " + elem.courseName + " " + elem.teacherName);
            elemCount++;
        }
        int choose1;
        int choose2;
        choose1 = scanner.nextInt();
        choose2 = scanner.nextInt();
        firstChoose = coursesArray[choose1-1];
        secondChoose = coursesArray[choose2-1];
        for(var elem: firstChoose.studentsNames)
        {
            for(var elem2: secondChoose.studentsNames)
            {
                if(elem.equals(elem2))
                {
                    System.out.println("Visit both courses:" + elem2);
                }
            }
        }
        System.out.println();
    }

    public void allCoursesVisitors()
    {
        Hashtable<String, ArrayList<String[]>> studentsInfoTable = new Hashtable<>();

        System.out.print("Choose list to check: ");
        Scanner scanner = new Scanner(System.in);
        int choose = scanner.nextInt();
        ArrayList <Course> choosenCourseArrayList;
        if(choose == 1)
        {
            choosenCourseArrayList = courseArrayList;
        }
        else
        {
            choosenCourseArrayList = courseArrayList2;
        }
        for(var elem: choosenCourseArrayList)
        {
            for(var elem2: elem.studentsNames)
            {
                if(!studentsInfoTable.containsKey(elem2))
                {
                    ArrayList<String[]> infoList = new ArrayList<>();
                    String[] infoLower = {elem.courseName, elem.teacherName};
                    infoList.add(infoLower);
                    studentsInfoTable.put(elem2, infoList);
                }
                else
                {
                    ArrayList<String[]> infoList = studentsInfoTable.get(elem2);
                    int count = 0;
                    for(var elem3: infoList)
                    {
                        if(!elem3[0].equals(elem.courseName) || !elem3[1].equals(elem.teacherName))
                        {
                            count++;
                        }
                    }
                    if(count == infoList.size())
                    {
                        infoList.add(new String[]{elem.courseName, elem.teacherName});
                    }
                }
            }
        }
        ArrayList <String> allCoursesListeners = new ArrayList<>();
        Set<String> students = studentsInfoTable.keySet();
        ArrayList<String> courses = new ArrayList<>();
        for(var elemCourse: choosenCourseArrayList)
        {
            if(!courses.contains(elemCourse.courseName))
            {
                courses.add(elemCourse.courseName);
            }
        }
        for(var elemStudent: students)
        {
            ArrayList<String> coursesLocal = new ArrayList<>();
            for (var elemCourse: studentsInfoTable.get(elemStudent))
            {
                if(!coursesLocal.contains(elemCourse[0]))
                {
                    coursesLocal.add(elemCourse[0]);
                }
            }
            if(courses.size() == coursesLocal.size())
            {
                allCoursesListeners.add(elemStudent);
            }
        }
        System.out.println("All courses listeners:");
        for (var elem : allCoursesListeners)
        {
            System.out.println(elem);
        }
        System.out.println();
    }

    public void tablePrint()
    {
        Hashtable<String, ArrayList<String[]>> studentsInfoTable = new Hashtable<>();

        System.out.print("Choose table to print: ");
        Scanner scanner = new Scanner(System.in);
        int choose = scanner.nextInt();
        ArrayList <Course> choosenCourseArrayList;
        if(choose == 1)
        {
            choosenCourseArrayList = courseArrayList;
        }
        else
        {
            choosenCourseArrayList = courseArrayList2;
        }
        for(var elem: choosenCourseArrayList)
        {
            for(var elem2: elem.studentsNames)
            {
                if(!studentsInfoTable.containsKey(elem2))
                {
                    ArrayList<String[]> infoList = new ArrayList<>();
                    String[] infoLower = {elem.courseName, elem.teacherName};
                    infoList.add(infoLower);
                    studentsInfoTable.put(elem2, infoList);
                }
                else
                {
                    ArrayList<String[]> infoList = studentsInfoTable.get(elem2);
                    int count = 0;
                    for(var elem3: infoList)
                    {
                        if(!elem3[0].equals(elem.courseName) || !elem3[1].equals(elem.teacherName))
                        {
                            count++;
                        }
                    }
                    if(count == infoList.size())
                    {
                        infoList.add(new String[]{elem.courseName, elem.teacherName});
                    }
                }
            }
        }
        Set<String> studentNamesSet = studentsInfoTable.keySet();
        for(var elem: studentNamesSet)
        {
            System.out.println("Student: " + elem);
            System.out.println("Student list of courses and teachers: ");
            for(var elem2: studentsInfoTable.get(elem))
            {
                String paddedName = elem + "              ".substring(elem.length());
                String padded = elem2[0] + "                ".substring(elem2[0].length());
                System.out.println(paddedName + "|| " + padded + "|| Teacher is: " + elem2[1]);
            }
            System.out.println();
        }
    }

    public void readFromFile(String path)
    {
        try
        {
            Scanner scanner = new Scanner(new File(("E:\\Project\\Semestr 5 Cross-programming\\Lab 3\\list.txt")));
            String line;
            while (scanner.hasNextLine())
            {
                line = scanner.nextLine();
                if(line.equals("*"))
                {
                    if(scanner.hasNextLine())
                    {
                        line = scanner.nextLine();
                        String teacherName = line;
                        String courseName = scanner.nextLine();
                        ArrayList<String> studentsName = new ArrayList<>();
                        while(!(line = scanner.nextLine()).equals("*"))
                        {
                            studentsName.add(line);
                        }
                        courseArrayList.add(new Course(courseName, teacherName, studentsName));
                    }
                }
            }
            scanner.close();
        }
        catch (Exception exception) {
            System.out.println("There is exception in file reading: " + exception);
        }
    }
}
